// Jorge Luis Zamudio Osuna     |       6 - 4       |       Ing. en TI

package TallerMecánico;

import javax.swing.JOptionPane;

public class RegistroVehículo extends javax.swing.JInternalFrame {
    // Creación de objetos:
    Registro reg = new Registro();
    VehículoCarga vehículo = new VehículoCarga();
    // Variables para los resultados:
        float pago;
        float impuesto;
        float total;
    // Función principal:
    public RegistroVehículo() {
        initComponents();
        this.resize(521, 474);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlInformacion = new javax.swing.JPanel();
        btnCerrar = new javax.swing.JButton();
        txtNumServicio = new javax.swing.JTextField();
        lblTítulo = new javax.swing.JLabel();
        lblFecha = new javax.swing.JLabel();
        lblDescripción = new javax.swing.JLabel();
        txtDescripción = new javax.swing.JTextField();
        txtTipoServicio = new javax.swing.JTextField();
        txtModelo = new javax.swing.JTextField();
        lblModelo = new javax.swing.JLabel();
        lblMarca = new javax.swing.JLabel();
        txtMarca = new javax.swing.JTextField();
        lblToneladas = new javax.swing.JLabel();
        txtToneladas = new javax.swing.JTextField();
        lblTipoServicioNombre = new javax.swing.JLabel();
        cmbTipoServicio = new javax.swing.JComboBox<>();
        lblTipoMotor = new javax.swing.JLabel();
        cmbTipoMotor = new javax.swing.JComboBox<>();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        pnlCostoMantenimiento = new javax.swing.JPanel();
        lblCostoMantenimiento = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblPago = new javax.swing.JLabel();
        lblImpuesto = new javax.swing.JLabel();
        txtPago = new javax.swing.JTextField();
        txtImpuesto = new javax.swing.JTextField();
        txtPagar = new javax.swing.JTextField();
        lblTipoServicio = new javax.swing.JLabel();
        lblNumServicio = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();

        getContentPane().setLayout(null);

        pnlInformacion.setLayout(null);

        btnCerrar.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        btnCerrar.setText("CERRAR");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        pnlInformacion.add(btnCerrar);
        btnCerrar.setBounds(370, 380, 120, 50);

        txtNumServicio.setEnabled(false);
        pnlInformacion.add(txtNumServicio);
        txtNumServicio.setBounds(140, 50, 40, 22);

        lblTítulo.setBackground(new java.awt.Color(102, 0, 204));
        lblTítulo.setFont(new java.awt.Font("Segoe UI Black", 1, 18)); // NOI18N
        lblTítulo.setForeground(new java.awt.Color(102, 0, 204));
        lblTítulo.setText("Registro de vehículo");
        pnlInformacion.add(lblTítulo);
        lblTítulo.setBounds(120, 10, 200, 26);

        lblFecha.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblFecha.setText("Fecha");
        pnlInformacion.add(lblFecha);
        lblFecha.setBounds(200, 50, 40, 20);

        lblDescripción.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblDescripción.setText("Descripción vehículo");
        pnlInformacion.add(lblDescripción);
        lblDescripción.setBounds(20, 80, 120, 20);

        txtDescripción.setEnabled(false);
        pnlInformacion.add(txtDescripción);
        txtDescripción.setBounds(150, 80, 210, 22);

        txtTipoServicio.setFont(new java.awt.Font("Segoe UI Light", 2, 12)); // NOI18N
        txtTipoServicio.setText("seleccione el tipo de servicio");
        txtTipoServicio.setEnabled(false);
        txtTipoServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTipoServicioActionPerformed(evt);
            }
        });
        pnlInformacion.add(txtTipoServicio);
        txtTipoServicio.setBounds(150, 110, 210, 22);

        txtModelo.setEnabled(false);
        pnlInformacion.add(txtModelo);
        txtModelo.setBounds(200, 210, 160, 22);

        lblModelo.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblModelo.setText("Modelo");
        pnlInformacion.add(lblModelo);
        lblModelo.setBounds(90, 210, 50, 20);

        lblMarca.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblMarca.setText("Marca");
        pnlInformacion.add(lblMarca);
        lblMarca.setBounds(90, 240, 50, 20);

        txtMarca.setEnabled(false);
        pnlInformacion.add(txtMarca);
        txtMarca.setBounds(200, 240, 160, 22);

        lblToneladas.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblToneladas.setText("Toneladas");
        pnlInformacion.add(lblToneladas);
        lblToneladas.setBounds(80, 270, 60, 20);

        txtToneladas.setEnabled(false);
        pnlInformacion.add(txtToneladas);
        txtToneladas.setBounds(200, 270, 160, 22);

        lblTipoServicioNombre.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblTipoServicioNombre.setText("Tipo Servicio");
        pnlInformacion.add(lblTipoServicioNombre);
        lblTipoServicioNombre.setBounds(60, 110, 80, 20);

        cmbTipoServicio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2" }));
        cmbTipoServicio.setEnabled(false);
        pnlInformacion.add(cmbTipoServicio);
        cmbTipoServicio.setBounds(250, 150, 60, 22);

        lblTipoMotor.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblTipoMotor.setText("Tipo Motor");
        pnlInformacion.add(lblTipoMotor);
        lblTipoMotor.setBounds(160, 180, 80, 20);

        cmbTipoMotor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4" }));
        cmbTipoMotor.setEnabled(false);
        pnlInformacion.add(cmbTipoMotor);
        cmbTipoMotor.setBounds(250, 180, 60, 22);

        btnNuevo.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        btnNuevo.setText("NUEVO");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        pnlInformacion.add(btnNuevo);
        btnNuevo.setBounds(370, 30, 120, 50);

        btnGuardar.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        pnlInformacion.add(btnGuardar);
        btnGuardar.setBounds(370, 100, 120, 50);

        btnMostrar.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        btnMostrar.setText("MOSTRAR");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        pnlInformacion.add(btnMostrar);
        btnMostrar.setBounds(370, 170, 120, 50);

        btnLimpiar.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        btnLimpiar.setText("LIMPIAR");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        pnlInformacion.add(btnLimpiar);
        btnLimpiar.setBounds(370, 240, 120, 50);

        btnCancelar.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        btnCancelar.setText("CANCELAR");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        pnlInformacion.add(btnCancelar);
        btnCancelar.setBounds(370, 310, 120, 50);

        pnlCostoMantenimiento.setBackground(new java.awt.Color(204, 204, 204));
        pnlCostoMantenimiento.setLayout(null);

        lblCostoMantenimiento.setFont(new java.awt.Font("Segoe UI Black", 1, 14)); // NOI18N
        lblCostoMantenimiento.setText("Costo de Mantenimiento");
        pnlCostoMantenimiento.add(lblCostoMantenimiento);
        lblCostoMantenimiento.setBounds(80, 10, 190, 20);

        lblTotal.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblTotal.setText("Total a Pagar:     $");
        pnlCostoMantenimiento.add(lblTotal);
        lblTotal.setBounds(10, 100, 100, 20);

        lblPago.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblPago.setText("Calculo Pago:     $");
        pnlCostoMantenimiento.add(lblPago);
        lblPago.setBounds(10, 40, 130, 20);

        lblImpuesto.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblImpuesto.setText("Calculo Impuesto:     $");
        pnlCostoMantenimiento.add(lblImpuesto);
        lblImpuesto.setBounds(10, 70, 130, 20);

        txtPago.setEnabled(false);
        pnlCostoMantenimiento.add(txtPago);
        txtPago.setBounds(150, 40, 180, 22);

        txtImpuesto.setEnabled(false);
        pnlCostoMantenimiento.add(txtImpuesto);
        txtImpuesto.setBounds(150, 70, 180, 22);

        txtPagar.setEnabled(false);
        pnlCostoMantenimiento.add(txtPagar);
        txtPagar.setBounds(150, 100, 180, 22);

        pnlInformacion.add(pnlCostoMantenimiento);
        pnlCostoMantenimiento.setBounds(10, 300, 350, 130);

        lblTipoServicio.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblTipoServicio.setText("Tipo Servicio");
        pnlInformacion.add(lblTipoServicio);
        lblTipoServicio.setBounds(160, 150, 80, 20);

        lblNumServicio.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        lblNumServicio.setText("No. Servicio");
        pnlInformacion.add(lblNumServicio);
        lblNumServicio.setBounds(60, 50, 80, 20);

        txtFecha.setEnabled(false);
        pnlInformacion.add(txtFecha);
        txtFecha.setBounds(240, 50, 120, 22);

        getContentPane().add(pnlInformacion);
        pnlInformacion.setBounds(0, 0, 510, 440);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtTipoServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTipoServicioActionPerformed

    }//GEN-LAST:event_txtTipoServicioActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        txtNumServicio.enable(true);
        txtFecha.enable(true);
        txtDescripción.enable(true);
        cmbTipoServicio.enable(true);
        cmbTipoMotor.enable(true);
        txtModelo.enable(true);
        txtMarca.enable(true);
        txtToneladas.enable(true);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        txtNumServicio.enable(false);
        txtFecha.enable(false);
        txtDescripción.enable(false);
        cmbTipoServicio.enable(false);
        cmbTipoMotor.enable(false);
        txtModelo.enable(false);
        txtMarca.enable(false);
        txtToneladas.enable(false);
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        int op = JOptionPane.showConfirmDialog(this, "¿Realmente quieres cerrar?", "Vehículos", JOptionPane.YES_NO_OPTION);

        if (op == JOptionPane.YES_OPTION) {

            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        txtNumServicio.setText("");
        txtFecha.setText("");
        txtDescripción.setText("");
        cmbTipoServicio.setSelectedItem(1);
        cmbTipoMotor.setSelectedItem(1);
        txtModelo.setText("");
        txtMarca.setText("");
        txtToneladas.setText("");
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // Campos: 
        txtNumServicio.setText(Integer.toString(reg.getNumServicio()));
        txtFecha.setText(reg.getFechaServicio());
        txtDescripción.setText(reg.getDescripciónAutomóvil());
        cmbTipoServicio.setSelectedItem(reg.getTipoServicio());
        cmbTipoMotor.setSelectedItem(vehículo.getTipoMotor());
        txtModelo.setText(vehículo.getModelo());
        txtMarca.setText(vehículo.getMarca());
        txtToneladas.setText(Float.toString(vehículo.getNumCarga()));
        // Resultados:
        txtPago.setText(Float.toString(pago));
        txtImpuesto.setText(Float.toString(impuesto));
        txtPagar.setText(Float.toString(total));
        // Tipo de servicio:
        switch (cmbTipoServicio.getSelectedIndex()) {
                case 0 -> txtTipoServicio.setText("Reparación");
                case 1 -> txtTipoServicio.setText("Mantenimiento");
                default -> {}
            }
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
    /*GUARDAR VALORES*/    
        // Número de Servicio:
        int numServ = Integer.parseInt(txtNumServicio.getText());
        reg.setNumServicio(numServ);
        // Fecha:
        String fecha = txtFecha.getText();
        reg.setFechaServicio(fecha);
        // Descripción:
        String Desc = txtDescripción.getText();
        reg.setDescripciónAutomóvil(Desc);
        // Tipo de Servicio:
        int tipoServ = cmbTipoServicio.getSelectedIndex() + 1;
        reg.setTipoServicio(tipoServ);
        // Tipo de Motor:
        int tipoMot = cmbTipoMotor.getSelectedIndex() + 1;
        vehículo.setTipoMotor(tipoMot);
        // Modelo:
        String mod = txtModelo.getText();
        vehículo.setModelo(mod);
        // Marca:
        String mar = txtMarca.getText();
        vehículo.setMarca(mar);
        // Toneladas:
        float ton = Float.parseFloat(txtToneladas.getText());
        vehículo.setNumCarga(ton);
    /*RESULTADOS*/
        // Atributos faltantes:
        reg.setVehículo(vehículo);
        reg.setCostoBaseMantenimiento(500);
        // Cálculos
        pago = reg.costoServicio(reg.getCostoBaseMantenimiento());
        impuesto = reg.costoImpuesto(reg.getCostoBaseMantenimiento());
        total = pago + impuesto;
    }//GEN-LAST:event_btnGuardarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbTipoMotor;
    private javax.swing.JComboBox<String> cmbTipoServicio;
    private javax.swing.JLabel lblCostoMantenimiento;
    private javax.swing.JLabel lblDescripción;
    private javax.swing.JLabel lblFecha;
    private javax.swing.JLabel lblImpuesto;
    private javax.swing.JLabel lblMarca;
    private javax.swing.JLabel lblModelo;
    private javax.swing.JLabel lblNumServicio;
    private javax.swing.JLabel lblPago;
    private javax.swing.JLabel lblTipoMotor;
    private javax.swing.JLabel lblTipoServicio;
    private javax.swing.JLabel lblTipoServicioNombre;
    private javax.swing.JLabel lblToneladas;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTítulo;
    private javax.swing.JPanel pnlCostoMantenimiento;
    private javax.swing.JPanel pnlInformacion;
    private javax.swing.JTextField txtDescripción;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtModelo;
    private javax.swing.JTextField txtNumServicio;
    private javax.swing.JTextField txtPagar;
    private javax.swing.JTextField txtPago;
    private javax.swing.JTextField txtTipoServicio;
    private javax.swing.JTextField txtToneladas;
    // End of variables declaration//GEN-END:variables
}
