// Jorge Luis Zamudio Osuna     |       6 - 4       |       Ing. en TI

package TallerMecánico;

public class VehículoCarga extends Vehículo{
    /*ATRIBUTOS*/
    private float numCarga;
    /*CONSTRUCTORES*/
    // Vacío:
    public VehículoCarga() {
        super();
        this.numCarga = 0;
    }
    // Parámetros:
    public VehículoCarga(int nS, int tM, String marK, String modL, float nC) {
        super(nS, tM, marK, modL);
        this.numCarga = nC;
    }
    // Copia:
    public VehículoCarga(VehículoCarga brumBrum) {
        super(brumBrum);
        this.numCarga = brumBrum.numCarga;
    }
    /*ENCAPSULAMIENTO*/
    // Número de toneladas:
    public float getNumCarga() {
        return numCarga;
    }
    public void setNumCarga(float numCarga) {
        this.numCarga = numCarga;
    }
    /*ENCAPSULAMIENTO-Override*/
    // Número de Serie:
    @Override
    public int getNumSerie() {
        return numSerie;
    }
    @Override
    public void setNumSerie(int numSerie) {
        this.numSerie = numSerie;
    }
    // Tipo de Motor:
    @Override
    public int getTipoMotor() {
        return tipoMotor;
    }
    @Override
    public void setTipoMotor(int tipoMotor) {
        this.tipoMotor = tipoMotor;
    }
    // Marca:
    @Override
    public String getMarca() {
        return marca;
    }
    @Override
    public void setMarca(String marca) {
        this.marca = marca;
    }
    // Modelo:
    @Override
    public String getModelo() {
        return modelo;
    }
    @Override
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
}
