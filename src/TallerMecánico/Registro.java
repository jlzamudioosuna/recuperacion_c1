// Jorge Luis Zamudio Osuna     |       6 - 4       |       Ing. en TI

package TallerMecánico;

public class Registro {
    /*ATRIBUTOS*/
    private int numServicio;
    private String fechaServicio;
    private int tipoServicio;
    private VehículoCarga vehículo;
    private String descripciónAutomóvil;
    private float costoBaseMantenimiento;
    /*CONSTRUCTORES*/
    // Vacío:
    public Registro() {
        this.numServicio = 0;
        this.fechaServicio = "DD/MM/2024";
        this.tipoServicio = 1;
        this.vehículo = new VehículoCarga();
        this.descripciónAutomóvil = "";
        this.costoBaseMantenimiento = 0;
    }
    // Parámetros:
    public Registro(int nS, String fS, int tS, VehículoCarga v, String dA, float CBM) {
        this.numServicio = nS;
        this.fechaServicio = fS;
        this.tipoServicio = tS;
        this.vehículo = v;
        this.descripciónAutomóvil = dA;
        this.costoBaseMantenimiento = CBM;
    }
    // Copia:
    public Registro(Registro reg) {
        this.numServicio = reg.numServicio;
        this.fechaServicio = reg.fechaServicio;
        this.tipoServicio = reg.tipoServicio;
        this.vehículo = reg.vehículo;
        this.descripciónAutomóvil = reg.descripciónAutomóvil;
        this.costoBaseMantenimiento = reg.costoBaseMantenimiento;
    }
    /*ENCAPSULAMIENTO*/
    // Número de Servicio:
    public int getNumServicio() {
        return numServicio;
    }
    public void setNumServicio(int numServicio) {
        this.numServicio = numServicio;
    }
    // Fecha de Servicio:
    public String getFechaServicio() {
        return fechaServicio;
    }
    public void setFechaServicio(String fechaServicio) {
        this.fechaServicio = fechaServicio;
    }
    // Tipo de Servicio:
    public int getTipoServicio() {
        return tipoServicio;
    }
    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }
    // Vehículo:
    public VehículoCarga getVehículo() {
        return vehículo;
    }
    public void setVehículo(VehículoCarga vehículo) {
        this.vehículo = vehículo;
    }
    // Descripción del Automovil:
    public String getDescripciónAutomóvil() {
        return descripciónAutomóvil;
    }
    public void setDescripciónAutomóvil(String descripciónAutomóvil) {
        this.descripciónAutomóvil = descripciónAutomóvil;
    }
    // Costo Base de Mantenimiento: 
    public float getCostoBaseMantenimiento() {
        return costoBaseMantenimiento;
    }
    public void setCostoBaseMantenimiento(float costoBaseMantenimiento) {
        this.costoBaseMantenimiento = costoBaseMantenimiento;
    }
    /*IMPLEMENTACIONES*/
    public float costoServicio(float costo) {
        float incremento = 0;
        switch (this.getVehículo().getTipoMotor()) {
            case 1 -> incremento = costo * 0.25f;
            case 2 -> incremento = costo * 0.50f;
            case 3 -> incremento = costo * 1.50f;
            case 4 -> incremento = costo * 2;
            default -> {}
        }
        float total = costo + incremento;
        return total;
    }
    public float costoImpuesto(float costo) {
        float costoServicio = this.costoServicio(costo);
        float impuesto = costoServicio * 0.16f;
        return impuesto;
    }
    
}
