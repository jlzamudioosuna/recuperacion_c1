// Jorge Luis Zamudio Osuna     |       6 - 4       |       Ing. en TI

package TallerMecánico;

public abstract class Vehículo {
    /*ATRIBUTOS*/
    protected int numSerie;
    protected int tipoMotor;
    protected String marca;
    protected String modelo;
    /*CONSTRUCTORES*/
    // Vacío:
    public Vehículo() {
        this.numSerie = 0;
        this.tipoMotor = 1;
        this.marca = "";
        this.modelo = "";
    }
    // Parámetros:
    public Vehículo(int nS, int tM, String marK, String modL) {
        this.numSerie = nS;
        this.tipoMotor = tM;
        this.marca = marK;
        this.modelo = modL;
    }
    // Copia:
    public Vehículo(Vehículo rumRum) {
        this.numSerie = rumRum.numSerie;
        this.tipoMotor = rumRum.tipoMotor;
        this.marca = rumRum.marca;
        this.modelo = rumRum.modelo;
    }
    /*ENCAPSULAMIENTO*/
    // Número de serie:
    public int getNumSerie() {
        return this.numSerie;
    }
    public void setNumSerie(int numSerie) {
        this.numSerie = numSerie;
    }
    // Tipo Motor:
    public int getTipoMotor() {
        return tipoMotor;
    }
    public void setTipoMotor(int tipoMotor) {
        this.tipoMotor = tipoMotor;
    }
    // Marca:
    public String getMarca() {
        return marca;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    // Modelo:
    public String getModelo() {
        return modelo;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
}
